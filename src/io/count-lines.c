#include <stdio.h>

int main(){
	count_lines();
	return 0;
}

void count_lines(){
	int c=0, nl=0, nws=0, nc=0;
	while ((c = getchar()) != EOF){
		if (c == '\n'){
			++nl;
		}
		else if (c == ' '){
			++nws;
			++nc;
		}
		else if (c == '\t'){
			++nws;
			++nc;
		}
		else
			++nc;
	}
	printf("\nYou typed %d lines, %d spaces and/or tabs, and %d characters\n", nl, nws, nc);
}
