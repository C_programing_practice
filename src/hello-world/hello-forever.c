#include <stdio.h>	//This includes the
			//standared input
			//output library.

main()			//This is the main
			//function which
			//needs to be put
			//at the beggining
			//of every program.

{			//This the main
			//opening bracket.

	for(;;)		//This is a for 
			//loop. Inside the
			//paranthesese, you
			//may notice that
			//there are two 
			//semi colons. The
			//two semi colons
			//mean that there
			//are no conditions,
			//thus the loop goes
			//on forever.

	{		//This is the opening
			//bracket for the for loop.

		printf("Hello World!!!\n");	//This prints
						//the Hello World
						//thingy like we
						//want it too.
						//Because it is 
						//encapsulated in
						//this for loop, it
						//wiil do this forever.

	}		//This is the closing
			//bracket for the for loop.

}		//This bracket
		//ends the program.
