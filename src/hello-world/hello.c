#include <stdio.h>

main()

{

/*
 The printf command is made for printing stuff out to the console. 
 It prints whatever is in the quotes, which is in this case, Hello World.
 If you look, you will see a \n within the quotes. The \n is a newline character.
 The backslash character is kind of special. It does different things 
 depending on what the precceding character is. In this case, it is n.
 The n stands for a newline. When this program is compiled, it gets
 the program to print Hello World and then a newline.
*/

 printf("Hello World!!!\n");

}
