#include <stdio.h>

/* This example program prints all command line arguments passed to it */ 

int main (
		/* The `argument count', the number of command line arguments */
		int argc,
		/* The `argument vector', a pointer to an array of string
		   pointers to the individual command line arguments, of which
		   the last one is a NULL pointer.
		   Please note that argv[0] is the file name of the program */
		char ** argv
	 )
{
	int i; /* needed in the for loop */

	/* see loops/for.c */
	for (i = 0; i < argc; i++)
		/* printf: see the hello world example */
		printf ("Argument %i: `%s'\n", i, argv[i]);

	return 0;
}
