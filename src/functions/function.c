#include <stdio.h>	//This includes the standard library.

int func(int num;);	//This line basically is the prototype for the func fuction. It must be similar to. ||
			//										    ||
int main(){		//This is main, the place where the program goes.				    ||
func(5);			//This executes the func function that we created.				    ||
}	//Ends main.	   //   /|==========================================================================//
int func(int num){// <==========|/	//This is basically like main. Instead of saying what the
					//program does, though, we say what the func function does.
printf("%d\n", num);	//Makes the func function print num, and then a newline.
}
