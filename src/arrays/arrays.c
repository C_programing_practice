#include <stdio.h>	//This includes the
			//header file that
			//we need to print
			//to the console.

int main(){		//This is the little
			//declaration of main
			//that we need in each
			//program we make.

int array[10], i;	//This creates an
			//integer called
			//array. It does
			//not only do that,
			//but we can store ten
			//arrays in here.

i = 0;		//This statement assigns
		//the value of one to the
		//integer we created called i.

while (i <= 9){	//This is a handy while
			//loop. What it says in
			//plain english is that
			//as long as "i" is less
			//than or equal to 10, do
			//the statements in braces.

	array[i] = i;		//This may be a little
				//confusing at first, but
				//this takes the array
				//array[i] (if i = 1,
				//than it is array[1],
				//if i = 2, it is array[2],
				//and so on), and gives
				//it the value of i.
				//This way, every array
				//will be the same as it's number.

	++i;		//This adds 1 to i.
}
for (i = 1;i <= 10;++i)		//In english: make i = 1, and as long as i is less than or equal to 10, add 1 to i and execute the following statements.
	printf("array[%d] equals %d\n", i, array[i]);		//If you do not understand this, check
								//out some of the other programs first.

return 0;	//You should know what this is.

}	//The program ends.
