#define TIME 500000
#include <stdio.h>	//This include teh standard input output library,
			//which we need to print to the console.
#include <unistd.h>
main()        //This is the main, function, which we need in every program.

{	//Inside these brackets is where
	//the actual program goes.

	int a;		//This creates an integer variable
			//with the name of a.

	a = 0;		//This makes a
			//equal to the
			//number 0.

	while (a <= 1)		//This is the while loop. 
				//If the test is true, than
				//the loop repeats. If it
				//is false, than it 
				//discontinues the loop.
	{
		printf("This\n")/*This prints this*/;usleep(TIME);printf("is\n");usleep(TIME);printf("the\n");usleep(TIME);printf("sentence\n");usleep(TIME);printf("that\n");
		usleep(TIME);printf("never\n");usleep(TIME);printf("ends\n\n\n");
		
	};
}	//This is
	//where the
	//program
	//actually
	//ends.


	//And this
	//is a 
	//tutorail
	//on the
	//while
	//function.
