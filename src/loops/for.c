#include <stdio.h>//This includes the standard input output library. 
		  //We need this to output stuff to the console.
main(){        //This is creates the clean function,
	       //which is required in every c program.

int count; 	//This creates the variable called count. 
		//In this case, the variable is an integer.

for (count = 0; count <= 100; count = count + 1){                   //This line creaates a loop. It assigns the value of 0 to count, it loops as long as count is less
  								    //than 100, and the value of count increases by one with each loop.

printf("The wheels on the bus go round and round, round and round, round and round. The wheels on the bus gor round and round, all through the town.\n");	//This command echoes the sentence
																				//which is in quotes.
sleep(1);	//This command makes the program pause for one second.
		//Remember that it is nested in a loop, so that the
		//loop will have an interval of one second between 
		//each of the 100 loops.
};	//this closes the for loop.

}//This signals that this is the end of the program.
